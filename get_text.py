#!/usr/bin/env python
import re

from gimpfu import *


text_match = re.compile(r'\(text "(.*[^\\])"\)')

def text_from_images():
    print gimp.image_list()
    for image in gimp.image_list():
        get_text(image)

def text_from_file(filename='test'):
    img = pdb.gimp_file_load(filename, filename)
    get_text(img)
    pdb.gimp_image_delete(img)

def get_text(img):
    for layer in img.layers:
        for parasite in layer.parasite_list():
            if parasite == 'gimp-text-layer':
                match = text_match.search(layer.parasite_find(parasite).data)
                if match:
                    s = match.group(1)\
                        .replace(r'\n', "\n")\
                        .replace(r'\"', "\"")
                    print s
    

register(
        "text_from_images",
        "Get text from text layers from all images",
        "Get text from text layers from all images",
        "Iakov Davydov",
        "Iakov Davydov",
        "2009",
        "<Toolbox>/Xtns/Languages/Python-Fu/Text/From _all",
        "",
        [],
        [],
        text_from_images)

register(
        "text_from_file",
        "Get text from text layers from file",
        "Get text from text layers from file",
        "Iakov Davydov",
        "Iakov Davydov",
        "2009",
        "<Toolbox>/Xtns/Languages/Python-Fu/Text/From _file",
        "",
        [
        (PF_STRING, 'filename', 'Filename', 'test'),
        ],
        [],
        text_from_file)


main()
